import dotenv from "dotenv";
import { Bot, InlineKeyboard } from "grammy";

dotenv.config();

const { BOT_TOKEN } = process.env;

const bot = new Bot(BOT_TOKEN!);

// You can now register listeners on your bot object `bot`.
// grammY will call the listeners when users send messages to your bot.

// Handle the /start command.
bot.command(
	"start",
	async (ctx) => await ctx.reply("Welcome! Up and running.")
);
// Handle other messages.

bot.command(
	"about",
	async (ctx) =>
		await ctx.reply("Обо мне", {
			reply_markup: new InlineKeyboard().webApp(
				"grammY website",
				"https://192.168.0.110:5173/"
			),
		})
);

bot.command(
	"test",
	async (ctx) =>
		await ctx.reply("Обо мне", {
			reply_markup: new InlineKeyboard().webApp(
				"grammY website",
				"https://revenkroz.github.io/telegram-web-app-bot-example/index.html"
			),
		})
);

bot.on("message", async (ctx) => {
	console.log(ctx.message.web_app_data);
	const data = ctx.message.web_app_data?.data;
	return await ctx.reply(data || "No data");
});

// Start the bot.
bot.start();
