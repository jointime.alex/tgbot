import { createTheme } from '@mui/material';
import './App.css';
import { Form } from './components/Form/Form';
import { ThemeProvider } from '@emotion/react';
import { useState } from 'react';



const theme = createTheme({
  palette: {
    primary: {
      main: '#3f51b5',
    },
    secondary: {
      main: '#f50057',
    },
  },
  typography: {
    fontFamily: 'Roboto, sans-serif',
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
  },
  // other theme options here
});
function App() {

  return (
    <>
  <ThemeProvider theme={theme}>
    <Form/>
  </ThemeProvider>

  
    </>

  )
}

export default App
