import { TextField, Button } from "@mui/material";
import { ChangeEventHandler, FormEventHandler, useState } from "react";
import './Form.css';




type FormData = {
    surname: string;
    name: string;
    fathersName: string;
    [key: string]: string; // index signature
}
export const Form = () => {

    const [formData, setFormData] = useState<FormData>({
        surname: '',    
        name: '',
        fathersName: '',
    });



    const [inputColors, setInputColors] = useState <{ [key:string]: 'primary' | 'secondary' | 'error'}>({
        surname: 'primary', 
        name: 'primary',
        fathersName: 'primary'
    });

    const [showContent, setShowContent] = useState(false);

    const handleShowContent = () => {
        setShowContent(!showContent);
    };

    const handleInputChange: ChangeEventHandler<HTMLInputElement> = (event) => {
        const {id, value} = event.target; 

        setFormData ((prevData) => ({
            ...prevData, 
            [id]: value,
        }));


    };

    const handleSubmit: FormEventHandler<HTMLFormElement> = (event) => {
        event.preventDefault();
        const isFormValid = Object.values(formData).every(value => value !== '');
        if (isFormValid) {
            handleShowContent();
            const newInputColors = {} as {[key: string]: 'primary' | 'secondary' | 'error'};
            for (const key in formData) {
                newInputColors[key] = 'primary'
            }
            setInputColors(newInputColors);
        } else {
            const newInputColors = {} as {[key: string]: 'primary' | 'secondary' | 'error'};
            for (const key in formData) {
                if (formData[key] === '') {
                    newInputColors[key] = 'secondary';
                    if (key === 'surname') {
                        alert ('Please, enter your surname');
                    } else if (key === 'name') {
                        alert ('Please, enter your name');
                    } else if (key === 'fathersName') {
                        alert ('Please, enter your father\'s name');
                    }
                } else {
                    newInputColors[key] = 'primary'
                }
            }
            setInputColors(newInputColors);
        }
    };


    return (
    <div>
       {showContent ? <Content formData={formData}/> : (
                            <form className="app-form" onSubmit={handleSubmit}>
                            <div className="input-div">
                        <TextField color={inputColors.surname} onChange={handleInputChange} fullWidth label="Surname" id="surname" className="input-field surname"/>
                            </div>
                            <div className="input-div">
                            <TextField color={inputColors.name} onChange={handleInputChange} fullWidth label="Name" id="name" className="input-field name"/>
                            </div>
                            <div className="input-div">
                            <TextField color={inputColors.fathersName} onChange={handleInputChange} fullWidth label="Father's name " id="fathersName" className="input-field fathers-name" />
                            </div>
                        
                        <Button className='app-form_button' type='submit' variant="contained" disableElevation>
                        Save changes
                        </Button>
                          </form>
       )}
    </div>

    )

};



const Content = ({formData}: {formData: FormData}) => {
    const {name, surname, fathersName} = formData;
    return (
        <table className="data-table">
          <tr>
            <th>Surname</th>
            <td>{surname}</td>
          </tr>
          <tr>
            <th>Name</th>
            <td>{name}</td>
          </tr>
          <tr>
            <th>Father's name</th>
            <td>{fathersName}</td>
          </tr>
        </table>
    )
}


